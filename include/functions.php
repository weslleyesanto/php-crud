<?php
//Arquivo criado para criar funções que seram usadas dentro do sistema

/*################################################################################################'
'########################### INICIO FUNÇÕES PRINCIPAIS DO SISTEMA ################################'
'################################################################################################*/

function trataData($string, $tipo = 0) {
	if ($string && empty($string) == false) {
		switch ($tipo) {
			case 1: 
			$ex = explode('/', $string);
							//$dia, $mes, $ano
			if(validarData($ex[0], $ex[1], $ex[2])){
				return $ex[2] . '-' . $ex[1] . '-' . $ex[0];
			}else{
				return false;
			}
			break;
			
			case 2:
			$ex = explode('-', substr($string, 0, 10));
			$horario = substr($string, 11, strlen($string)); 
							//$dia, $mes, $ano
			if(validarData($ex[2], $ex[1], $ex[0])){
				return $ex[2] . '/' . $ex[1] . '/' . $ex[0];
			}else{
				return false;
			}
			break;
			
			case 3:
			$ex_data = explode('-', substr($string, 0, 10));
			$horario = substr($string, 11, strlen($string));
			return $ex_data[2] . '/' . $ex_data[1] . '/' . $ex_data[0] . ' ' . $horario;
			break;
			
			case 4:
			$ex_data = explode('-', substr($string, 0, 10));
			$horario = substr($string, 11, strlen($string));
			return $ex_data[2] . '-' . $ex_data[1] . '-' . $ex_data[0] . ' ' . $horario;
			break;
			index:
			$ex = explode('-', $string);
			return $ex[2] . '-' . $ex[1] . '-' . $ex[0];
			break;
		}
	}
}

function validarData($dia, $mes, $ano){

	if($dia <= 0 OR $dia >=13){//VERIFICA MES SE É MENOR IGUAL A ZERO OU MAIOR IGUAL A 13 
		return false;
	}else if($mes <= 0 OR $mes >= 32){ // VERICA SE DIA É MENOR IGUAL A ZERO OU MAIOR IGUAL A 32
		return false;
	}
	else if($ano <= 0){ // VERICA SE ANO É MENOR IGUAL A ZERO
		return false;	
	}else{
		return true;
	}
}


function delTree($dir) { 
	$files = array_diff(scandir($dir), array('.','..')); 
	foreach ($files as $file) { 
		(is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file"); 
	} 
	return rmdir($dir); 
}

function limpar($string){

	$string = trim($string);
	$string =str_replace("'","",$string);//aqui retira aspas simples <'>
	$string =str_replace("\\","",$string);//aqui retira barra invertida<\\>
	$string =str_replace("UNION","",$string);//aqui retiro  o comando UNION <UNION>

	$banlist = array(" insert", " select", " update", " delete", " distinct", " having", " truncate", "replace"," handler", " like", " as ", "or ", "procedure ", " limit", "order by", "group by", " asc", " desc","'","union all", "=", "'", "(", ")", "<", ">", " update", "-shutdown",  "--", "'", "#", "$", "%", "¨", "&", "'or'1'='1'", "--", " insert", " drop", "xp_", "*", " and");
	// ---------------------------------------------
	if(preg_match("/[a-zA-Z0-9]+/", $string)){
		$string = trim(str_replace($banlist,'', strtolower($string)));
	}

	return $string;

}

function validaEmail($email){
	$er = "/^(([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}){0,1}$/";
	if (preg_match($er, $email)){
		return true;
	} else {
		return false;
	}
}

function select($conn, $TABELA, $PARAM = false, $WHERE, $INNER = false, $GROUP_BY = false, $ORDER_BY = false, $DEBUG = false){
		//VERIFICA SE PARAM É FALSE'
	if($PARAM == false){
		$PARAM = "";
	}

		//VERIFICA SE INNER É FALSE'
	if($INNER == false){
		$INNER = "";
	}

		//VERIFICA SE GROUP_BY É FALSE'
	if ($GROUP_BY == false){
		$GROUP_BY = "";
	}

	//VERIFICA SE GROUP_BY É FALSE'
	if ($ORDER_BY == false){
		$ORDER_BY = "";
	}

		//MONTANDO QUERY'
	$QUERY = "SELECT * " . $PARAM . " FROM " . $TABELA . $INNER . $WHERE . $GROUP_BY.$ORDER_BY;

	if($DEBUG){
		die('###SELECT>>> '.$QUERY);
	}

	return $conn->query($QUERY);
}

function update($conn, $TABELA, $SET, $WHERE, $DEBUG = false){

	//MONTANDO QUERY'
	$QUERY = 'UPDATE ' . $TABELA . $SET .$WHERE;

	if($DEBUG){
		die('###UPDATE>>> '.$QUERY);
	}

	return $conn->query($QUERY);
}

function delete($conn, $TABELA, $WHERE, $DEBUG = false){

		//MONTANDO QUERY'
	$QUERY = 'DELETE FROM '. $TABELA . $WHERE;

	if($DEBUG){
		die('###DELETE>>> '.$QUERY);
	}

	return $conn->query($QUERY);
}

function insert($conn, $TABELA, $PARAM, $LAST_ID = FALSE, $DEBUG = false){

	$QUERY = 'INSERT INTO ' . $TABELA . $PARAM;

	if($DEBUG){
		die('###INSERT>>> '.$QUERY);
	}

	if($LAST_ID){
		$conn->query($QUERY);
		return $conn->lastInsertId($LAST_ID);
	}

	return $conn->query($QUERY);
}

function criarPasta($caminho, $pasta){
	
	if(!@file_exists($caminho."/".$pasta)){
		if(!@mkdir($caminho."/".$pasta, 0777)){
			return true;
		}else{
			return false;
		}
	}
	return false;
}

function sendEmail($remetente = false ,$remetente_nome,$destino,$copia =false ,$assunto,$corpo){
	
	//Inicia a classe PHPMailer
	$mail = new PHPMailer();

	// Define os dados do servidor e tipo de conexão
	$mail->IsSMTP(); // Define que a mensagem será SMTP
	$mail->Host = "smtp.kbrtec.com.br"; // Endereço do servidor SMTP
	$mail->SMTPAuth = true; // Usa autenticação SMTP? (opcional)
	$mail->Username = 'weslley.santo@kbrtec.com.br'; // Usuário do servidor SMTP
	$mail->Password = 'wesley2015so'; // Senha do servidor SMTP

	// Define o remetente
	if($remetente != false){
		$mail->From = $remetente; // Seu e-mail
		$mail->FromName = "No Reply"; // Seu nome
	}else{
		$mail->From = "no-reply@kbrtec.com.br"; // Seu e-mail
		$mail->FromName = "No Reply"; // Seu nome
	}

	// Define os destinatário(s)
	$mail->AddAddress($destino, $remetente_nome);
	//$mail->AddCC('ciclano@site.net', 'Ciclano'); // Copia
	//$mail->AddBCC('fulano@dominio.com.br', 'Fulano da Silva'); // Cópia Oculta

	// Define os dados técnicos da Mensagem
	$mail->IsHTML(true); // Define que o e-mail será enviado como HTML
	$mail->CharSet = 'UTF-8'; // Charset da mensagem (opcional)

	// Define a mensagem (Texto e Assunto)
	$mail->Subject  = $assunto; // Assunto da mensagem
	$mail->Body = $corpo;
	
	// Define os anexos (opcional)
	//$mail->AddAttachment("c:/temp/documento.pdf", "novo_nome.pdf");  // Insere um anexo

	// Envia o e-mail
	return $mail->Send();

	// Limpa os destinatários e os anexos
	$mail->ClearAllRecipients();
	$mail->ClearAttachments();

}

/*################################################################################################'
'########################### FINAL FUNÇÕES PRINCIPAIS DO SISTEMA ################################'
'################################################################################################'

'################################################################################################'
'################################## INICIO FUNÇÕES DO SISTEMA ####################################'
'################################################################################################*/


function get_perfil($conn){
	
	$TABELA = "tb_perfil p";
	$ORDER_BY = " ORDER BY nm_perfil ASC";
	$INNER = " LEFT JOIN tb_categoria c ON c.id_categoria = p.id_categoria ";
				//$conn, TABELA, PARAM, WHERE, INNER, GROUP_BY, ORDER_BY, DEBUG
	return  select($conn, $TABELA, false, false, $INNER, false, $ORDER_BY, false);

}

function get_categoria($conn){
	$TABELA = "tb_categoria ";
	$ORDER_BY = " ORDER BY nm_categoria ASC";
				//$conn, TABELA, PARAM, WHERE, INNER, GROUP_BY, ORDER_BY, DEBUG
	return select($conn, $TABELA, false, false, false, false, $ORDER_BY, false);
}

function verificaCategoria($conn, $nm_categoria, $id_categoria = false){

	$TABELA = "tb_categoria";
	$WHERE = " WHERE nm_categoria = '".$nm_categoria."'";
	
	if($id_categoria){
		$WHERE .= " AND id_categoria <> ".$id_categoria;
	}

									//$conn,TABELA, PARAM, WHERE, INNER, GROUP_BY, ORDER_BY, DEBUG
	$retorno = select($conn, $TABELA, false, $WHERE, false, false, false, false);
	
	return $retorno->rowCount();

}

function verificaEmail($conn, $nm_email, $id_perfil = false){

	$TABELA = "tb_perfil";
	$WHERE = " WHERE nm_email = '".$nm_email."'";
	
	if($id_perfil){
		$WHERE .= " AND id_perfil <> ".$id_perfil;		
	}
									//$conn, TABELA, PARAM, WHERE, INNER, GROUP_BY, ORDER_BY, DEBUG
	$retorno = select($conn, $TABELA, false, $WHERE, false, false, false, false);
	
	return $retorno->rowCount();
}

/*'################################################################################################'
'#################################### FINAL FUNÇÕES DO SISTEMA ####################################'
'##################################################################################################*/
?>