<?php
include_once('include\config.php');

$REDIRECIONA = "index.php";

$categorias = get_categoria($conn);

$selecionado = "";
$visualizar = false;
$ALTERAR = false;
$id_categoria_perfil = "";
$pasta_thumb = "/thumb/";

if($_GET["q"] == "s"){
    if($_GET["acao"] == "add"){
        $title = "Adicionar Perfil";
        $nm_perfil = "";
        $email = "";
        $dt_nascimento = "";
        $ds_foto = "";
        $id_categoria = "";
        $id_perfil = "0";
    }elseif($_GET["acao"] == "alterar" AND $_GET["id"] != ""){

       if(!is_numeric($_GET["id"])){
           header("location:{$REDIRECIONA}");
       } else{

        $title = "Alterar Perfil";
        $ALTERAR = true;
        $id_perfil = limpar($_GET["id"]);

        //QUERY VERIFICAR SE CATEGORIA EXISTEM COM ESSE ID'
        $TABELA = "tb_perfil p";
        $PARAM = ", p.id_categoria as id_categoria_perfil ";
        $WHERE = " WHERE id_perfil = ". $id_perfil;
        $INNER = " LEFT JOIN tb_categoria c ON c.id_categoria = p.id_categoria ";
                                    //TABELA, PARAM, WHERE, INNER, GROUP_BY, ORDER_BY, DEBUG
        $execute_query = select($conn, $TABELA, $PARAM, $WHERE, $INNER, false, false, false);

        if($execute_query->rowCount() > 0){//VERIFICA SE É MAIOR QUE ZERO
            foreach($execute_query as $row){
                $id_perfil = $row["id_perfil"];
                $nm_perfil = utf8_encode($row["nm_perfil"]);
                $email = $row["nm_email"];
                $dt_nascimento = trataData($row["dt_nascimento"], 2);
                $ds_foto = $row["ds_foto"];
                $id_categoria_perfil = utf8_encode($row["id_categoria_perfil"]);
            }
            //FINAL FOREACH
        }else{ //SE NÃO ENCONTROU VOLTA PRA LISTAGEM'
        header("location:{$REDIRECIONA}");
    }
        //FINAL VERIFICA SE RETORNOU ALGO DO SELECT'
}
//VERIFICA SE ID É DO TIPO NUMERICO
}elseif($_GET["acao"] == "visualizar" AND $_GET["id"] != ""){
    
    if(!is_numeric($_GET["id"])){
       header("location:{$REDIRECIONA}");
   } else{

    $visualizar = true;
    $title = "Visualizar Perfil";
    $id_perfil = limpar($_GET["id"]);

        //QUERY VERIFICAR SE CATEGORIA EXISTEM COM ESSE ID'
    $TABELA = "tb_perfil p";
    $PARAM = ", p.id_categoria as id_categoria_perfil ";
    $WHERE = " WHERE id_perfil = ". $id_perfil;
    $INNER = " LEFT JOIN tb_categoria c ON c.id_categoria = p.id_categoria ";
                                    //$conn,TABELA, PARAM, WHERE, INNER, GROUP_BY, ORDER_BY, DEBUG
    $execute_query = select($conn, $TABELA, $PARAM, $WHERE, $INNER, false, false, false);

        if($execute_query->rowCount() > 0){ //VERIFICA SE TRUE
            foreach ($execute_query as $row) {
                $id_perfil = $row["id_perfil"];
                $nm_perfil = utf8_encode($row["nm_perfil"]);
                $email = $row["nm_email"];
                $dt_nascimento = trataData($row["dt_nascimento"], 2);
                $ds_foto = $row["ds_foto"];
                $id_categoria_perfil = $row["id_categoria_perfil"];
                $nm_categoria_perfil = utf8_encode($row["nm_categoria"]);
            }
            //FINAL FOREACH
        }else{//SE NÃO ENCONTROU VOLTA PRA LISTAGEM'
        header("location:{$REDIRECIONA}");
    } 
        //FINAL VERIFICA SE RETORNOU ALGO DO SELECT'
}
//VERIFICA SE ID É DO TIPO NUMERICO
}else{
    header("location:{$REDIRECIONA}");
}
    //FINAL IF TIPO DE AÇÃO'
}else{
    header("location:{$REDIRECIONA}");
}
//FINAL IF Q IGUAL A S' 
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?=$title?></title>

</head>

<body>

    <div id="wrapper">

       <?php include_once('include\menu.php'); ?>

       <div id="page-wrapper">

        <div class="container-fluid">

            <h1><?=$title?></h1>

            <div id="alert" style="display:none;"> </div>

            <form role="form" name="form_post_perfil" method="post" id="form_post_perfil" enctype="multipart/form-data">

                <?php if(!$visualizar): ?>
                    <label>Nome</label>
                    <input type="text" maxlength="100" required="required" placeholder="Nome" id="nm_perfil" name="nm_perfil" value="<?=$nm_perfil?>"/>
                    <br/><br/>

                    <label>E-mail</label>
                    <input type="text" maxlength="100" required="required" placeholder="seu@email.com" id="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" value="<?=$email?>" />
                    <br/><br/>

                    <label>Data de nascimento</label>
                    <input type="text" required="required" placeholder="DD/MM/YYYY" id="dt_nascimento" name="dt_nascimento" value="<?=$dt_nascimento?>" />
                    <br/><br/>

                    <label>Foto</label>
                    <input type="file" id="foto" name="foto" ds-foto="<?=$ds_foto?>" />

                    <?php if($ALTERAR): ?>
                        <br/><br/>
                        <img src="<?=$RELATIVO_IMAGEM_PERFIL.$id_perfil.$pasta_thumb.$ds_foto?>" alt="<?=$nm_perfil?>" title="<?=$nm_perfil?>" id="caminho_foto" />
                    <?php endif; ?>
                    <br/><br/>
                    <label>Categoria</label>
                    <select name="id_categoria" id="id_categoria" >
                        <option value="">Sem categoria</option>
                        <?php 
                        if($categorias)://VERIFICA SE TRUE 
                        foreach($categorias as $row):
                            $pk_categoria = $row["id_categoria"];
                        $selecionado = $pk_categoria == $id_categoria_perfil ? 'selected=selected"' : '';
                        ?>
                        <option value="<?=$pk_categoria?>" <?=$selecionado?>>
                            <?=utf8_encode($row["nm_categoria"])?>
                        </option>
                        <?php
                        endforeach;//FINAL FOREACH
                        endif;//FINAL _GET CATEGORIAS'
                        ?>
                    </select>
                    <br/><br/>
                    <input type="hidden" id="id_perfil" name="id_perfil" value="<?=$id_perfil?>"/>
                    <button type="submit" name="Salvar" id="btnSalvar">Salvar</button>
                <?php else: ?>
                    <label>Nome: <?=$nm_perfil?></label>
                    <br/><br/>
                    <label>E-mail: <?=$email?></label>
                    <br/><br/>
                    <label>Data de nascimento : <?=$dt_nascimento?></label>
                    <br/><br/>
                    <label>Foto: <img src="<?=$RELATIVO_IMAGEM_PERFIL.$id_perfil.$pasta_thumb.$ds_foto?>" alt="<?=$nm_perfil?>" title="<?=$nm_perfil?>" id="caminho_foto" /></label>
                    <br/><br/>
                    <label>Categoria: <?=$nm_categoria_perfil?></label>
                <?php endif; ?>
            </form>

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<?php include_once('include\js.php'); ?>
<script src="assets/js/perfil.js"></script>
</body>

</html>
