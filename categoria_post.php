<?php
include_once('include\config.php');

if(isset($_POST) AND $_POST['nm_categoria'] != ''){

	$nm_categoria = limpar(utf8_decode($_POST["nm_categoria"]));
	$id_categoria = limpar($_POST["id_categoria"]);
	$modulo = "Categoria";
	$url = "categoria_list.php";
	$retorno = array();

    if($id_categoria != "0"){ //SE ID FOR DIFERENTE DE ZERO É UMA ALTERAÇAO
    	
    	$verificar_categoria = verificaCategoria($conn, $nm_categoria, $id_categoria);

    	if($verificar_categoria > 0){
    		$retorno = array('res'=>'error', 'msg'=>"Já existe uma {$modulo} com esse nome!");
    	}else{

	    	//INICIO UPDATE DA CATEGORIA
    		$TABELA = "tb_categoria";
    		$PARAM = " SET nm_categoria = '".$nm_categoria."'";
    		$WHERE = " WHERE id_categoria = " . $id_categoria;
					//$conn,TABELA, PARAM, WHERE, DEBUG'
    		$execute_update = update($conn, $TABELA, $PARAM, $WHERE, false);

    		if($execute_update){
    			$retorno = array('res'=>'ok', 'msg'=>"{$modulo} alterada com sucesso!", 'url' => $url);
    		}else{
    			$retorno = array('res'=>'error', 'msg'=>"Problema ao alterar {$modulo}!");
    		}
    	}
    	//FINAL VERIFICA CATEGORIA IS TRUE

    }else{ //SENAO É UMA INSERÇÃO'
    
    $verificar_categoria = verificaCategoria($conn, $nm_categoria, false);
    
    if($verificar_categoria > 0){
    	$retorno = array('res'=>'error', 'msg'=>"Já existe uma {$modulo} com esse nome!");
    }else{

    	//Salvar no banco de dados na tabela categoria
    	$TABELA = "tb_categoria";
    	$PARAM = "(nm_categoria) VALUES ('" . $nm_categoria ."')";

    	$insert_row = insert($conn, $TABELA, $PARAM, false, false);

		if($insert_row){ //se inseriu retorna TRUE
			$retorno = array('res' => 'ok', 'msg' => "{$modulo} cadastrada com sucesso!", 'url'=> $url);	
		}else{
			$retorno = array('res' => 'error', 'msg' => "Problema ao inserir nova {$modulo}!");	
		}
		//FINAL VERIFICA SE INSERIU NA TABELA CATEGORIA'
	}
	//FINAL VERIFICA CATEGORIA IS TRUE
}
    //FINAL IF VERIFICA SE É INSERT OU UPDATE'
echo(json_encode($retorno));
}else{
	header("location: categoria_list.php");
}
//FINAL VERIFICA METODO DE REQUISIÇÃO IGUAL A POST'

?>