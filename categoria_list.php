<?php include_once('include\config.php');
$execute_select = get_categoria($conn); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Categorias</title>
</head>
<body>

    <div id="wrapper">

    <?php include_once('include\menu.php'); ?>

     <div id="page-wrapper">

        <div class="container-fluid">

            <h1>Listagem Categorias</h1>

            <a href="categoria_form.php?q=s&acao=add" title="Adicionar Categoria">
                Adicionar
            </a>

            <div id="alert" style="display:none;"> </div>
            <table id="listaCategoria">
             <?php  if($execute_select->rowCount() > 0){ ?>
             <thead>
                <tr>
                    <th>#</th>
                    <th>Nome</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
               <?php foreach($execute_select as $row): ?>
               <tr data-id="<?=$row["id_categoria"]?>" modulo="categoria" page="categoria_list">
                <td><?=$row["id_categoria"]?></td>
                <td><?=utf8_encode($row["nm_categoria"])?></td>
                <td>
                    <a href="categoria_form.php?q=s&acao=alterar&id=<?=$row["id_categoria"]?>" title="Alterar" class="alterar">Alterar</a>
                    <a href="#" title="Excluir" class="excluir">Excluir</a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <?php
        }else{
        ?>
        <tr>
            <td colspan="6">Nenhum registro encontrado!</td>
        </tr>
        <?php }?>
    </table>

</div>
<!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<?php include_once('include\js.php'); ?>
<script src="assets/js/categoria.js"></script>
</body>

</html>
