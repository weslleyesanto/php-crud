var categoria = (function() {
	var C = {};

	C.setDHTML = function() {
		
		$('#form_post_categoria').submit( function(e){
			e.preventDefault();
		});

		$('#btnSalvar').on('click', function() {
			C.salvar();
		});

	};

	C.salvar = function() {

		var nm_categoria = $('#nm_categoria').val();
		var id_categoria = $('#id_categoria').val();
		var error = false;

		if (nm_categoria == '') {
			msg = "Preencha o nome da categoria!";
			C.alertError(msg);
			error = error ? error : true;
		}

		if(!error){
			var data = {
				nm_categoria:nm_categoria,
				id_categoria: id_categoria
			};

			$.post('categoria_post.php',data, function(ret){
				
				if(ret.res == 'ok'){
					alert(ret.msg);
					window.location.href = ret.url;
				}
				else{
					C.alertError(ret.msg);
					return false;
				}

			}, 'json');
		}
	}

	C.alertError = function(msg){
		$('#alert')
			.addClass('alert-danger')
			.css({'display':'block'})
			.html('<strong style="color:red">Error!</strong> '+msg);
	}


	$(function() {
		C.setDHTML();
	});

	return C;
})();