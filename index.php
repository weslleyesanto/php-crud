<?php include_once('include\config.php');

$execute_select = get_perfil($conn); 
$pasta_thumb = "/thumb/";
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Perfil</title>

</head>

<body>

    <div id="wrapper">

       <?php include_once('include\menu.php'); ?>

       <div id="page-wrapper">


        <div class="container-fluid">
            <h1>Listagem Perfil</h1>
            <div id="alert" style="display:none;"> </div>

            <div id="">

                <a href="perfil_form.php?q=s&acao=add" title="Adicionar Perfil">Adicionar</a>
                <table id="listaUsuarios">
                    <?php  if($execute_select->rowCount() > 0){ ?>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Thumb</th>
                            <th>Nome</th>
                            <th>E-mail</th>
                            <th>Data de nascimento</th>
                            <th>Categoria</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                     <?php foreach($execute_select as $row): 
                            $id_perfil = $row["id_perfil"];
                            $ds_foto = $row["ds_foto"];
                            $nm_categoria = utf8_encode($row["nm_categoria"]);
                     ?>
                            <tr data-id="<?=$id_perfil?>" modulo="perfil" page="index">
                                <td><?=$id_perfil?></td>
                                <td>
                        <img src="<?=$RELATIVO_IMAGEM_PERFIL.$id_perfil.$pasta_thumb.$ds_foto?>" alt="<?=$nm_perfil?>" title="<?=$nm_perfil?>" id="caminho_foto" />
                                </td>
                                <td><?=utf8_encode($row["nm_perfil"])?></td>
                                <td><?=$row["nm_email"]?></td>
                                <td><?=trataData($row["dt_nascimento"], 2)?></td>
                                <td><?=$nm_categoria = $nm_categoria == '' ? 'Sem categoria' : $nm_categoria ?></td>
                                <td>
                                    <a href="perfil_form.php?q=s&acao=alterar&id=<?=$id_perfil?>" title="Alterar" class="alterar">
                                        Alterar
                                    </a>
                                    <a href="perfil_form.php?q=s&acao=visualizar&id=<?=$id_perfil?>" title="Visualizar">Visualizar</a>
                                    <a href="#" title="Excluir" class="excluir">Excluir</a>
                                    <a href="#" title="Notificar" class="notificar">Notificar</a>
                                </td>
                            </tr>
                            <?php  
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                    <?php }else{ ?>
                    <tr>
                        <td colspan="6">Nenhum registro encontrado!</td>
                    </tr>
                    <?php }?>
                </div>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <?php include_once('include\js.php'); ?>
    <script src="assets/js/perfil.js"></script>
</body>

</html>
