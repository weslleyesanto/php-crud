<?php
include_once('include\config.php');
include_once('include\class.phpmailer.php');
include_once('include\class.smtp.php');
include_once('include\wideimage\WideImage.php');

$url = "index.php";

if (isset($_POST)) {

    //PEGANDO CAMPOS PASSADOS DO FORMULÁRIO PARA AS VARIÁVEIS'
	$foto          = @$_FILES["foto"];
	$ds_foto       = limpar($_POST["ds_foto"]);
	$nm_perfil     = limpar(utf8_decode($_POST["nm_perfil"]));
	$dt_nascimento = limpar(trataData($_POST["dt_nascimento"], 1));
	$id_perfil     = limpar($_POST["id_perfil"]);
	$id_categoria  = limpar($_POST["id_categoria"]);
	$modulo        = "Perfil";
	$TABELA        = "tb_perfil";
	$retorno       = array();

  if(!$dt_nascimento){
    $retorno = array('res' => 'error','msg' => 'Formato da data incorreto!');
    echo (json_encode($retorno));
    die();
  }

  if(!validaEmail($_POST["email"])){
    $retorno = array('res' => 'error','msg' => 'Preencha o E-mail corretamente!');
    echo (json_encode($retorno));
    die();
 }else{
    $email = limpar($_POST["email"]);
 }


    //CAMINHA DO ARQUIVO
	$file_path = "assets/imagem/perfil/";

    if ($ds_foto == "") { //INSERIR NOVO REGISTRO'

    if ($foto['size'] == "") {
    	echo ("<center>Por favor, indique um arquivo para upload.</center><br>");
    	die();
    } else {

      $verifica_email = verificaEmail($conn, $email, false);

      if($verifica_email > 0){
        $retorno = array('res'=>'error_email', 'msg'=>"Já existe um e-mail cadastrado com esse endereço!");
      }else{

       $nome_foto = limpar($foto['name']);
       $novo_nome = md5(date('Y-m-d H:i:s')).'.jpg';

            //PREPARANDO PARAMETRO PARA INSERIR NO BD
       $PARAM      = "(nm_email, nm_perfil, dt_nascimento, ds_foto, id_categoria) VALUES ('" . $email . "', '" . $nm_perfil . "', '" . $dt_nascimento . "', '" . $novo_nome . "', " . $id_categoria . ")";
            //$conn, $TABELA, $PARAM, $LAST_ID, $DEBUG
       $insert_row = insert($conn, $TABELA, $PARAM, 'id_perfil', false);

            if ($insert_row) { //se inseriu retorna TRUE

                //CRIAR A PASTA COM O ID DO USUÁRIO
            	$pasta_id = criarPasta($file_path, $insert_row);

                if (!$pasta_id) { //CRIOU A PASTA COM O ID'

                #INICIO UPLOAD DE IMAGEM PARA O SERVIDOR#'
                if (!@move_uploaded_file($foto['tmp_name'], $file_path . $insert_row . '/' . $novo_nome)) {
                	$retorno = array('res' => 'error','msg' => 'Problema ao mover arquivo!');
                }
                    #FINAL UPLOAD DE IMAGEM PARA O SERVIDOR#'

                $pasta_thumb = criarPasta($file_path . $insert_row, "/thumb/");

                if ($pasta_thumb) {
                	$retorno = array('res' => 'error','msg' => 'Problema ao criar pasta do THUMB!');
                } else {

                	$THUMB = $file_path . $insert_row . "/thumb/";
                        // Carrega a imagem a ser manipulada
                	$image = WideImage::load($file_path . $insert_row . '/' . $novo_nome);
                        // Redimensiona a imagem
                	$image = $image->resize(90, 90);
                        // Salva a imagem em um arquivo (novo ou não)
                	if (!$image->saveToFile($THUMB . '/' . $novo_nome)) {

                        #INICIO ENVIO DE E-MAIL#'
                		$nome_remetente = "KBRTEC";
                		$destino        = $email;
                		$assunto        = "Novo Cadastro";
                		$corpo          = "Você foi cadastrado com sucesso!";

                		$envio = sendEmail(false, $nome_remetente, $destino, FALSE, $assunto, $corpo);
                        #FINAL ENVIO DE E-MAIL#'

                		if (!$envio) {
                			$retorno = array('res' => 'error','msg' => 'Problema ao enviar e-mail!');
                		}

                            //FINAL VERIFICA SE ENVIOU E-MAIL'
                	} else {
                		//echo "aqui false";
                		$retorno = array('res' => 'error','msg' => 'Problema ao redimensionar imagem!');
                	}
                        //VERIFICA REDIMENSIONAR
                }
                    //FINAL VERIFICA SE CRIOU PASTA THUMB'
              } else {
               $retorno = array('res' => 'error','msg' => 'Problema ao criar pasta do perfil!');
             }
                //FINAL VERIFICA SE CRIOU A PASTA ID'
             $retorno = array('res' => 'ok', 'msg' => "{$modulo} cadastrado com sucesso!",'url' => $url);
           } else {
             $retorno = array('res' => 'error','msg' => "Problema ao inserir novo {$modulo}!");
           }
            //FINAL VERIFICA SE INSERIU NA TABELA'
         }
      //VERIFICA SE EXISTE E-MAIL IGUAL
       }
        //FINAL VERIFICA SE FOTO NÃO ESTÁ VAZIA'
     } elseif ($ds_foto != "") {

        if ($id_perfil != "0") { //'SE ID FOR DIFERENTE DE ZERO É UMA ALTERAÇAO

        $verifica_email = verificaEmail($conn, $email, $id_perfil);

        if($verifica_email > 0){
          $retorno = array('res'=>'error_email', 'msg'=>"Já existe um e-mail cadastrado com esse endereço!");
        }else{

            if ($foto['size'] != "") { //'ALTERAR DADOS E FAZER UM NOVO UPLOAD'

            $file = $file_path . $id_perfil . '/'. $ds_foto;
            $file_thumb = $file_path . $id_perfil . '/thumb/'.$ds_foto;
            
            if(file_exists($file)){
              unlink($file);
            }

            if(file_exists($file_thumb)){
             unlink($file_thumb);
           }

           $nome_foto = limpar($foto['name']);
           $novo_nome = md5(date('Y-m-d H:i:s')).'.jpg';
                #INICIO UPLOAD DE IMAGEM PARA O SERVIDOR#'
           if (!@move_uploaded_file($foto['tmp_name'], $file_path . $id_perfil . '/' . $novo_nome)) {
             $retorno = array('res' => 'error','msg' => 'Problema ao mover arquivo!');
           }
                #FINAL UPLOAD DE IMAGEM PARA O SERVIDOR#'

           $THUMB = $file_path . $id_perfil . "/thumb/";
                // Carrega a imagem a ser manipulada
           $image = WideImage::load($file_path . $id_perfil . '/' . $novo_nome);
                // Redimensiona a imagem
           $image = $image->resize(90, 90);
                // Salva a imagem em um arquivo (novo ou não)

           if ($image->saveToFile($THUMB . '/' . $novo_nome)) {

             $retorno = array('res' => 'error','msg' => 'Problema ao redimensiocar imagem!');
           }

         }
         //FINAL VERIFICA SIZE FOTO DIFERENTE DE VAZIO
         
    //INICIO UPDATE
         $PARAM = " SET nm_email = '" . $email . "'," . " nm_perfil = '" . $nm_perfil . "'," . " id_categoria = " . $id_categoria . ", dt_nascimento = '" . $dt_nascimento . "'";

         if ($foto['size'] != "") {
           $PARAM .= ", ds_foto = '" . $novo_nome . "'";
         }

         $WHERE          = " WHERE id_perfil = " . $id_perfil;
                    //conn, TABELA, PARAM, WHERE, DEBUG'
         $retorno_update = update($conn, $TABELA, $PARAM, $WHERE, false);

         if ($retorno_update) {
           $url     = "perfil_form.php?q=s&acao=alterar&id=" . $id_perfil;
           $retorno = array('res' => 'ok','msg' => "{$modulo} alterado com sucesso!",'url' => $url);
         } else {
           $retorno = array('res' => 'error','msg' => "Problema ao alterar {$modulo}!");
         }
        //FINAL VERIFICA SE FEZ UPDATE
       }
       //FINAL VERIFICA SE EXISTE E-MAIL IGUAL
     }
        //FINAL VERIFICA SE DS_FOTO É IGUAL A 0'
   }

   echo (json_encode($retorno));

 } else {
   header("location: {$url}");
 }
//FINAL VERIFICA METODO DE REQUISIÇÃO IGUAL A POST'
